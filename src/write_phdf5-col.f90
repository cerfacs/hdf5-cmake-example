
  SUBROUTINE hdf5p_check_retval ( retval,operation )

    IMPLICIT NONE

    INTEGER, INTENT(IN) :: retval
    CHARACTER(LEN=*), INTENT(IN) :: operation

    IF ( retval/=0 ) THEN
      !IF ( PRESENT(argument) ) THEN
      !  WRITE (*,*) 'Failed in hdf5p->',TRIM(operation),' ',TRIM(argument)
      !  CALL abort

      !ELSE
        WRITE (*,*) 'Failed in hdf5p->',operation
        CALL abort
      !END IF
    END IF

  END SUBROUTINE hdf5p_check_retval


  PROGRAM  writePHDF5


      USE hdf5
      USE IFPORT

      IMPLICIT NONE
      INCLUDE 'mpif.h'


!     HDF
      INTEGER(HID_T)    :: file_id       ! File identifier
      INTEGER(HID_T)    :: group_id      ! Group identifier
      INTEGER(HID_T)    :: dset_id       ! Dataset identifier
      INTEGER(HID_T)    :: filespace     ! Dataspace identifier in file
      INTEGER(HID_T)    :: memspace      ! Dataspace identifier in memory
      INTEGER(HID_T)    :: plist_id      ! Property list identifier

      INTEGER(HSIZE_T), DIMENSION(1) :: count
      INTEGER(HSIZE_T), DIMENSION(1) :: offset

      INTEGER           :: error

!     USER DEPENDENT
      INTEGER, DIMENSION(1)  :: ncount
      INTEGER, DIMENSION(1)  :: noffset
!     USER DEPENDENT

      CHARACTER(LEN=240) :: filename,outfile
      CHARACTER(LEN=240), DIMENSION(1:2) :: setname
      CHARACTER(LEN=240), DIMENSION(1:3) :: groupname
      INTEGER           :: rank = 1 ! dataset rank

      REAL(4), DIMENSION(:), ALLOCATABLE :: w
      DOUBLE PRECISION :: start_time, stop_time, write_time, rank_time
      INTEGER(HSIZE_T), DIMENSION(1)     :: dimsf  ! Datasetdims
      CHARACTER(LEN=20) :: chardum
      INTEGER :: nfiles

!     MPI
      INTEGER           :: mpierror,comm,info,mpi_size,mpi_rank,i, k
      CALL MPI_INIT(mpierror)
      comm = MPI_COMM_WORLD
      info = MPI_INFO_NULL
      CALL MPI_COMM_SIZE(comm,mpi_size,mpierror)
      CALL MPI_COMM_RANK(comm,mpi_rank,mpierror)

      outfile = "test-col"
      groupname(1) = "titi"
      groupname(2) = "tuti"
      groupname(3) = "toto"
      setname(1) = "rho"
      setname(2) = "velocity"

      nfiles = 1

      dimsf = 4*4*1024*128

!     Each process defines dataset in memory and writes it to the hyperslab
!     in the file.

      count(1)  = dimsf(1)/mpi_size
      ncount(1) = dimsf(1)/mpi_size
      count(1)  = ncount(1)

      offset(1) = mpi_rank * count(1)

!    Initialize data buffer with trivial data.

      CALL srand(99999)
      ALLOCATE (w(count(1)))
      DO i=1,count(1)
        w(i) = rand()
      END DO
      !w = mpi_rank*1.0


!     Initialize FORTRAN predefined types.
      IF ( mpi_rank == 0 ) PRINT*, "Starting ...."

      start_time = MPI_Wtime()

      call h5open_f ( error )

      DO i=1,nfiles
! Create file using MPI-IO
      CALL INT2STRING(i,chardum)
      filename = TRIM(outfile)//"--"//TRIM(chardum)//".h5"

      CALL hdf5p_create_file ( TRIM(filename),comm,info,file_id )
        DO k=1,3
!       Create the group
        call h5gcreate_f ( file_id,groupname(k),group_id,error )

!       WRITE DATA
        CALL hdf5p_write_vectorf ( offset(1)+1,count(1),w,dimsf(1),setname(1),group_id )

!       WRITE DATA
        CALL hdf5p_write_vectorf ( offset(1)+1,count(1),w,dimsf(1),setname(2),group_id )

!       Close the group
        call h5gclose_f ( group_id,error )

        END DO

!     Close the file
      CALL h5fclose_f(file_id,error)

      END DO

!     Close FORTRAN predefined datatypes.
      CALL h5close_f(error)
      IF (  mpi_rank == 0 ) PRINT*, "... Ended"

      stop_time = MPI_Wtime()

      write_time = stop_time - start_time
      rank_time = write_time

      call MPI_ALLREDUCE(write_time,rank_time,1, &
     &                    MPI_DOUBLE_PRECISION,MPI_MAX, &
     &                    MPI_COMM_WORLD,mpierror)

       IF ( mpi_rank == 0 ) PRINT*, " COL:", mpi_size, write_time


      DEALLOCATE(w)

      CALL MPI_FINALIZE(mpierror)


      END PROGRAM writePHDF5



  SUBROUTINE hdf5p_create_file ( filename,comm,info,hfile )

    USE hdf5

    IMPLICIT NONE

    CHARACTER(LEN=240), INTENT(IN) :: filename
    INTEGER, INTENT(IN) :: comm
    INTEGER, INTENT(IN) :: info
    INTEGER(HID_T), INTENT(OUT) :: hfile

    INTEGER(HID_T) :: plist_id
! Property list identifier
    INTEGER(HID_T) :: file_id
    INTEGER :: error


!   Setup file access property list with parallel I/O access.

    CALL h5pcreate_f ( H5P_FILE_ACCESS_F, plist_id, error )

    CALL hdf5p_check_retval ( error,"creating property list for file creation" )

    CALL h5pset_fapl_mpio_f ( plist_id, comm, info, error )

    CALL hdf5p_check_retval ( error,"setting mpio and comm to property list" )

!   Create the file collectively.

    CALL h5fcreate_f ( filename, H5F_ACC_TRUNC_F, file_id, error, &
    access_prp = plist_id )

    CALL hdf5p_check_retval ( error,"creating file")

    CALL h5pclose_f ( plist_id, error )

    CALL hdf5p_check_retval ( error,"closing property list" )

    hfile = file_id

  END SUBROUTINE hdf5p_create_file


  SUBROUTINE hdf5p_write_vectorf ( wnd_start,wnd_size,vec_data,vec_size,ds_name,group )

    USE hdf5

    IMPLICIT NONE

    INTEGER(HID_T),INTENT(IN)              ::wnd_start
    INTEGER(HID_T),INTENT(IN)              ::wnd_size
    REAL(4),INTENT(IN)             ::vec_data(wnd_size)
    INTEGER(HID_T),INTENT(IN)              ::vec_size
    CHARACTER(LEN=240),INTENT(IN)  ::ds_name
    INTEGER(HID_T),INTENT(IN)              ::group

    INTEGER error
    INTEGER(HSIZE_T) hdf_size(1),hdf_offset(1)
    INTEGER(HID_T) fspace_id,dset_id,grp_id,mspace_id
    INTEGER(HID_T) :: plist_id                                          ! Property list identifier

    INTEGER rank


    rank = 1

    grp_id = group


! Create the data space for the  dataset.

    hdf_size(1)=vec_size
    CALL h5screate_simple_f(rank, hdf_size, fspace_id, error)
    CALL hdf5p_check_retval(error,"failed creating simple dataspace(file)")


! Create the dataset with default properties.

    CALL h5dcreate_f(grp_id, ds_name, H5T_NATIVE_REAL, fspace_id, dset_id, error)
    CALL hdf5p_check_retval(error,"failed creating simple dataset: "//TRIM(ds_name))


    CALL h5sclose_f(fspace_id, error)
    CALL hdf5p_check_retval(error,"failed closing dataspace(file)")

! Each process defines dataset in memory and writes it to the hyperslab
! in the file.

    hdf_size(1) = wnd_size
    hdf_offset(1) = 0
    CALL h5screate_simple_f(rank, hdf_size, mspace_id, error)
    CALL hdf5p_check_retval(error,"failed creating simple dataspace(mem)")


! Select hyperslab in the file.

    hdf_size(1) = wnd_size
    hdf_offset(1) = wnd_start-1

    CALL h5dget_space_f(dset_id, fspace_id, error)
    CALL hdf5p_check_retval(error,"failed getting dataset dataspace")


    CALL h5sselect_hyperslab_f (fspace_id, H5S_SELECT_SET_F, hdf_offset, hdf_size, error)
    CALL hdf5p_check_retval(error,"failed selecting hyperslab")



! Create property list for collective dataset write

    CALL h5pcreate_f(H5P_DATASET_XFER_F, plist_id, error)
    CALL hdf5p_check_retval(error,"failed creating property list for collective write")

    CALL h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, error)
    CALL hdf5p_check_retval(error,"failed setting mpio_collective to property list")


! Write the dataset collectively.

    CALL h5dwrite_f(dset_id, H5T_NATIVE_REAL, vec_data, hdf_size, error,&
          file_space_id = fspace_id, mem_space_id = mspace_id, xfer_prp = plist_id)

    CALL hdf5p_check_retval(error,"failed writing dataset")


! Close dataspaces.

    CALL h5sclose_f(fspace_id, error)
    CALL hdf5p_check_retval(error,"failed closing file dataspace")

    CALL h5sclose_f(mspace_id, error)
    CALL hdf5p_check_retval(error,"failed closing mem dataspace")


! Close the dataset and property list.

    CALL h5dclose_f(dset_id, error)
    CALL hdf5p_check_retval(error,"failed closing dataset")

    CALL h5pclose_f(plist_id, error)
    CALL hdf5p_check_retval(error,"failed closing dataset property list")

  END SUBROUTINE hdf5p_write_vectorf


SUBROUTINE int2string ( k,string )
!
!***********************************************************************
!
!     Purpose: create a string from an integer
!
!     Input:  k
!
!     Output: string
!
!     Local:
!
!     Notes:
!
!     Parent: mean_values.F record.F postproc_minmax, output_multi
!
!     Author: A. Ruiz 15/03/2011
!
!     Modified: G. Staffelbach 06/07/2011 ( cleanning and Force size 20)
!
!***********************************************************************
!
  IMPLICIT NONE

! IN/OUT
  INTEGER, INTENT(IN) :: k
  CHARACTER(LEN=*), INTENT(OUT) :: string


! Transform k into a string
  WRITE(string,*) k

! Remove white space at the beginning of string
  string = ADJUSTL(string)


END SUBROUTINE int2string

